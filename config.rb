# First, require any additional compass plugins installed on your system.
require 'neat-compass'

disable_warnings = true

# Location of the your project's resources.
css_dir         = "css"
sass_dir        = "sass"
extensions_dir  = "scss/extensions"
images_dir      = "images"
javascripts_dir = "js"

http_path = "/"

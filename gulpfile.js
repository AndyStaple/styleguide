// Filename: gulpfile.js
// Selection: 1
// Plugins
// =======================================================
var gulp = require('gulp'),
		gutils = require('gulp-load-utils'),
		compass = require('gulp-compass'),
		imagemin = require('gulp-imagemin'),
		notify = require('gulp-notify'),
		livereload = require('gulp-livereload');

var server = livereload();

// Paths
// =======================================================
var paths = {
	scripts: ['js/*.js'],
	styles: ['sass/**/*.scss'],
	images: ['images/**/**'],
	html: ['*.html'],
	php: ['*.php'],
}


// Tasks
// =======================================================
gulp.task('compass', function() {
	gulp.src('sass/**/*.scss')
		.pipe(compass({
			config_file: 'config.rb',
			css: 'css',
			sass: 'sass'
		}))
		.pipe(gulp.dest('../'))
		.pipe(livereload());
		//.pipe(notify("Sass Compiled!"));
});

gulp.task('images', function() {
		gulp.src(paths.images)
				.pipe(imagemin({
					optimizationLevel: 5
				}))
				.pipe(gulp.dest('images'));
});

// Gulp Watching
gulp.task('watch', function() {
		gulp.watch('sass/**/*.scss', ['compass']);
		gulp.watch(paths.html).on('change', function(file) {
			server.changed(file.path);
		});
		gulp.watch(paths.php).on('change', function(file) {
			server.changed(file.path);
		});
});

gulp.task('default', ['compass', 'images']);
